# Movie api

## Authors
- Amine Ben Slama  
- Victoria Doe

## Software
- Node JS - Express  
- Typescript  
- PostgreSQL  
- Docker  
- Kubernetes  
- Terraform  

## Subject
An API that allows users to see the list of movies available in a theatre.  
Basic non authenticated users can aswell see a list of all the actors in these movies and perform basic operations such as finding an actor by its last name.    
Users can create an account to add reviews to movies and like or dislike them.  
Only movie owners have the possibility of adding and removing movies and actors.  
They can also moderate the reviews by deleting them but only if registered users have reported it at least three times.

## Queries
1. Get all actors   
2. Get actor by surname   
3. Get all movies  
4. Get movie by name  
5. Get all reviews of a movie  
6. Create movie  
7. Create actor  
8. Create review  
9. Register basic user  
10. Register movie owner  
11. Login basic user  
12. Login movie owner  
13. Update movie  
14. Like movie  
15. Report review  
16. Delete movie  
17. Delete review  

## Requirements solving

1. Input validation  

- Strictly and deeply validate the type of every input (params, querystring, body) at runtime before any processing  

We used io-ts for input validation.    
We have defined runtime types for each of our entites that contain their required and optional parameters.  
Each request body goes through a middleware that checks whether the JSON payload matches these runtime types. If so, the next middleware is called.  
If not, the request fails with status 400 and returns the incorrect fields of the payload.   
Moreover if any additional properties are added, the request will simply ignore them and perform the query with the required parameters only.    
[Payload validation](src/helpers/validate-payload.ts)

- Ensure the type of every input can be inferred by Typescript at any time and properly propagates across the app    

Each input parameter is resolved to a primitive type that can therefore be inferred at runtime (moreover there is no dependency between interfaces such as MovieOwner object in the Movie object).   

- Ensure the static and runtime input types are always synced  

Static types are built from runtime types and therefore synchronized.  
Dtos are created in the following manner :  
```typescript
  export type Actor = t.TypeOf<typeof RuntimeActor>;
```
[Types](src/helpers/validate-payload.ts) - lines 125 to 129   

2. Authentication and authorization  
 
Some endpoints are restricted to registered users and some endpoints are restricted to movie owners.  
We use the basic auth technique to log users in with their email and password and return a token that has a validity of 30 minutes to be used on auth-protected endpoints.  

- Check the current user is allowed to call this endpoint

We perform authorization checks on endpoints which require particular roles:   
When the user authenticates in the API they receive a token that contains an encoded payload.  
This payload contains their technical id (the one in the database).  
We decode this payload and check the properties inside.  
In case of a movie-owner restricted enpoint, we check that the payload has the **id_movie_owner property** (a registered user cannot have it).  
If so, we check in the table of all the movies owners in the database that a user with this id actually exists and that they have the role to perform this operation. If so we return the requested resource.   
If not we throw an Unauthorized error **(403 - Forbidden access)**.  
[Authorization](src/helpers/validate-privileges.ts) - line 131 to 167

- Check the current user is allowed to perform the action on a specific resource

Additionnaly to our authorization systems, movie owners are only allowed to perform update and deletion actions on the resources that they have created. For example, a movie owner cannot modify or delete another movie owner's movie.  
If a movie owner with an id equal to 4 tries to delete a movie that was created by a movie owner with an id equal to 3 (meaning they are two different persons) then the request fails with status 403.  
[Get logged in movie owner id](src/helpers/identification.ts)  
[Update movie](src/controllers/movie-controller.ts) - line 69 to 72  

- Did you build or use an authorisation framework, making the authorisation widely used in your code base?

We built our authorization system ourselves but followed a basic role-principle as described above.  
To be allowed to call an endpoint you need specific properties that are unique to a certain type of user. We check the presence of this properties to determine whether or not the user are who they claim to be (and therefore authorize them to access the resource). The authorization systems lies on a single method as described above, this method is available throughout the whole code.  

- Do you have any way to ensure authorisation is checked on every endpoint?

This is an example of a route in the application :  
```typescript
  router.post("/", [validateRequest, requireAuthentication, requireMovieOwnerRole], autoCatch(actorsController.createActor));
);
```
This route allows a movieOwner to create an actor.   
There are multiple moddleware that perform checks, each check should pass before the next one is called and all of them should pass for the controller to be called.   
Here, the authorization system is represented by the requireMovieOwnerRole function.  
Therefore whenever the route is triggered it will perform the following checks :
- [Make sure the requests accepts and sends JSON (and only JSON)](src/helpers/validate-request.ts)    
- [Make sure the user is authenticated (with the issued token)](src/helpers/validate-privileges.ts) line 87 to 91  
- Make sure the user has the role to perform the operation

If one of the checks does not pass, the controller is not called and the request fails with whatever status is sent depending on the thrown error. To make sure the authorization is called on every endpoint, we need to add this function to every route where it is needed.  

3. Secret management

- Use a hash for any sensitive data you do not need to store as plain text.  

No plain password is registered in the database. First we convert the password to base64 to add another level of encryption. Then we hash this password using the [bcrypt algorithm](src/helpers/validate-privileges.ts) line 16 to 18  

- Store your configuration entries in environment variables or outside the git scope.  

All sensitive information (databse credentials, secret for encoding passwords) in stored in an .env file that should never be committed to the remote.

- Do you provide a way to list every configuration entries ?  
Setup instructions, documentation, requireness... are appreciated

To run the project locally, first make sure you have Node and npm installed.  
Clone this [URL](https://gitlab.com/victoria-cs-doe/movie-api.git) 

In all cases you must first install all the node_modules by executing the following command :  
```bash
  npm install
```

To run the project in development mode :  
```bash
  npm run dev
```

To run the project in production mode : (npm run start will automatically build the project)  
```bash
  npm run start
```

To run the tests :
```bash
  npm run build
  npm run test
```

To run the tests with summary :
```bash
  npm run build
  npm run test:summary
```

You will also need to set the following environment variables in a .env file at the root of the project (do not wrap variables in quotes):      

| Environment Variable | Default Value |   Type    |
| -------------------- | ------------- | --------- |
| PORT                 | 4005          | integer   |
| SECRET               | hidden        | string    |
| NODE_ENV             | development   | string    |
| ROLLBAR_ACCESS_TOKEN | hidden        | string    |
| DATABASE_USER        | hidden        | string    |
| DATABASE_PASSWORD    | hidden        | string    |
| DATABASE_NAME        | hidden        | string    |
| DATABASE_HOST        | hidden        | string    |
| DATABASE_PORT        | hidden        | string    |
| TEST_PASSWORD_HASH   | hidden        | string    |

For the testing part, please create a .env.test file also at the root of the project and set the following variables:
    
| Environment Variable | Default Value |
| -------------------- | ------------- |
| DATABASE_USER        | hidden        |
| DATABASE_PASSWORD    | hidden        |
| DATABASE_NAME        | hidden        |

- Do you have a kind of configuration validation with meaningful error messages? 

If one of those environment variables is not set, the program won't launch and throw an error with the following message:  
  Missing mandatory environment variable (name of the variable)  
[Dotenv variables](src/lib/dotenv.ts)

4. Package management

**Do not use any package with less than 50k downloads a week**  
- Did you write some automated tools that check no unpopular dependency was installed? If yes, ensure it runs frequently  

- Properly use dependencies and devDevepencies in your package.json  

All typescript-related (@types, ts-node, ts-node-dev, typescript) dependencies are devDependencies because TypeScript is designed to be transpiled into JavaScript. Therefore in production, there is no remaning TypeScript but only javascript (hence why we don't need the types dependencies).  
All other dependencies (tests excluded) should be in dependencies. We chose to include test dependencies in devDependencies because according to our CI/CD pipeline, we will perform the tests in development environment.

5. Documentation

- Do you have automated documentation generation for your API (such as OpenAPI/Swagger...)?  
The documentation is generated with Postman, it is available at this [URL](https://documenter.getpostman.com/view/10491457/UVXokZDa)

- In addition to requireness and types, do you provide a comment for every property of your documentation?  

We provide a JSON with the types of each property and formats they have to be in if applicable.  

- Do you document the schema of responses (at least for success codes) and provide examples of payloads?  

We provide payloads of successful responses in all cases and sometimes we specify which kind of payloads to expect in case of request failure.  

- Is your documentation automatically built and published when a commit reach the develop or master branches?

Our documentation is synced with our development environment in Postman, therefore it does not depend on the version control files in GitLab. However, whenever there is a modification of the request (parameters, body etc.) in local, the deployed version is automatically updated.   

6. Error management  
  
- Do not expose internal application state or code (no sent stacktrace in production!)

We have defined a custom ErrorHandler with predefined http request errors. Whenever an http request error supposedly can happen, an error is thrown in the code with the right http status code and a message to describe what happened. For example, if a user requests a resource that doesn't exist, the code handles it and throws a 404 error.    
If the error is not of our custom types (and is therefore not handled by the code), then the error handler sends a 500 error (default error) with a generic message. This catches any unhandled promise rejection error.   
[Error handler](src/middleware/error-handler.ts)

Whenever a client error happens, the request fails and sends a JSON object of the following form:  
```json
    {
        "error": "Error message",
        "status": "Error status"
    }
```

There are 7 kind of custom errors :  
    - BadRequestError: status 400  
    - AuthenticationFailureError: status 401  
    - UnauthorizedError: status 403  
    - NotFoundError: status 404  
    - ResourceConflictError: status 409  
    - PreconditionRequiredError: status 428   
    - DatabaseError: status 500 (server error)   
[Custom errors](src/dtos/Exception.ts)

- Do you report errors to Sentry, Rollbar, Stackdriver… ?  

All errors are reported to Rollbar using a simple logger and and a personal acess token to an account.  
[Rollbar logger](src/middleware/rollbar.ts)  
![Rollbar account error logs](assets/images/rollbar.png?raw=true "Rollbar")  

7. Log management

We have created or own logger using winston to record information to log files and to the console.  

- Mention everything you put in place for a better debugging experience based on the logs collection and analysis.  

Each predicted error in the code is recorded into log files and to the console depending on the environment.   
Each day a new file is created so that they are easier to read.  
We have a specific file for error logs and another one for all other levels.  
[Logger](src/middleware/logger.ts) lines 36 to 66  

- Mention everything you put in place to ensure no sensitive data were recorded to the log.  

We do not directly send server's response, we predict the cases in which errors are likely to happen and send them back to the user an error with a custom status and custom message (hence no forwarded sensitive data).   
Moreover we have implemented 2 safety precautions :  
- [Logs are not sent to the console if we are in production environment](src/middleware/logger.ts) lines 60 to 66    
- We have defined 4 levels of logging (error, warn, info, http, debug). When set to a certain level, all the lower levels are also included. We perform a check of the current environment (development, test, production).  
If we are in development environment we set the level to debug (this means that all logs of lower types will be printed to log files and to the console). [If we are in production environment, only warn and error messages will be printed to log files](src/middleware/logger.ts) lines 13 to 16    

8. Code quality

- Did you put a focus on reducing code duplication?

We have tried as much as we could to use helpers that defined methods reusable thoughout the whole code and to make small functions (dao-layer for example) also resusable everywhere.   
We have also used a linter so that this type of error would be explicitely shown.  

- Eslint rules are checked for any pushed commit to develop or master branch.

We have set a pre-commit hook with husky that runs the following commands before any commit :  
```bash
  eslint --fix src
  prettier --write
  git add
```

9. Asynchronous first

- Always use the async implementations when available.
Throughout the code, in all http requests we have made our methods asynchronous :
```typescript
      async / await
``` 

- No unhandled promise rejections, no uncaught exceptions…  

First of all, esLint detects all of those errors and underlines them in red. This allows us to pay particular attention to them. Moreover we have an error handler that catches all kind of non customs errors and sends a 500 response so that no promise rejection error can be thrown.   

10. Automated tests

Please be aware that you need a good internet connection to run all the tests as each test only has 2000ms to run, otherwise you might need to increase mocha execution timeout for each method by adding the following parameter to the end of the npm test script : --timeout 10000 (increase it as mch as needed)  

- You implemented automated specs. Please provide a link to the more complete summary you have.  

[Summary](assets/test-report/mochawesome.pdf)  

- Your test code coverage is 75% or more. Please provide a link to the `istanbul` HTML coverage summary (or from a similar tool).  

Please see this summary as a measure of how many tests were implemented.  
[Summary](assets/test-report/mochawesome.pdf) 

- Do you run the test on a CD/CI, such as Github Action? Please provide a link to the latest test summary you have, hosted on Github Action or similar.  

(Please note that we have changed the way we implemented tests in the code, using async/await at first and done/then/catch because of timeout problems that weren't related to the actual time it took the request to complete. We weren't able to solve it and decided it was better to focus on writing tests even though it is not the best method).  
We have implemented a pipeline using the gitlab-ci.yml file that allows us to run a pipeline at each new commit. This pipeline performs installation of all the npm packages and builds the project.    
[Latest pipeline run](https://gitlab.com/victoria-cs-doe/movie-api/-/pipelines)    
[Pipeline script](.gitlab-ci.yml)    


### Deployment

1. Deployed application

The application is deployed on heroku, it is updated at every new commit on the develop branch :   
- https://movie-theatre-api.herokuapp.com 

2. Docker container

Build docker image:  
```bash
  docker build -t movie-api-container .
```
  
Run container from docker image:  
```bash
  docker run -p 4005:4005 -t movie-api-container
```
  
3. Kubernetes cluster

Create deployment:
```bash
  kubectl apply -f movie-api-deployment.yml
```

Create service:
```bash
  kubectl apply -f movie-api-service.yml
```

Get the http url of the application:
```bash
  minikube service movie-api --url
```

4. Terraform

Init terraform : 
```bash
  terraform init
```

Apply terraform : 
```bash
  terraform apply
```

Get http url of the application: 
```bash
  minikube service movie-api --url
```  

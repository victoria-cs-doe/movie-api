INSERT INTO MOVIE (title, duration, lang, subtitles, director, min_age, start_date, end_date, id_movie_owner) 
values ('PULP FICTION', 154, 'eng', true, 'Quentin Tarantino', 18, '1995-10-01', '1995-11-22', 2);
INSERT INTO MOVIE (title, duration, lang, subtitles, director, min_age, start_date, end_date, id_movie_owner) 
values ('JAWS', 124, 'eng', true, 'Steven Spielberg', '1975-10-01', '1975-11-22', 1);
INSERT INTO MOVIE (title, duration, lang, subtitles, director, min_age, start_date, end_date, id_movie_owner) 
values ('DUNKERQUE', 107, 'eng', true, 'Christopher Nolan', '2021-10-10', '2021-11-15', 3);
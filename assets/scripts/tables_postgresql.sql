------------------------------------------------------------
-- Table: movie owner
------------------------------------------------------------
CREATE TABLE public.movie_owner(
	id_movie_owner   SERIAL NOT NULL ,
	email            VARCHAR (50) NOT NULL ,
	password         VARCHAR (255) NOT NULL  ,
	role			 VARCHAR (255) DEFAULT 'owner',
	CONSTRAINT movie_owner_PK PRIMARY KEY (id_movie_owner)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: user
------------------------------------------------------------

CREATE TABLE public.user(
	id_user 	SERIAL NOT NULL ,
	email 		VARCHAR (50) NOT NULL ,
	password	VARCHAR (255) NOT NULL ,
	role	    VARCHAR (255) DEFAULT 'registered',
	CONSTRAINT user_PK PRIMARY KEY (id_user)

)WITHOUT OIDS;

------------------------------------------------------------
-- Table: movie
------------------------------------------------------------
CREATE TABLE public.movie(
	id_movie         SERIAL NOT NULL ,
	title            VARCHAR (50) NOT NULL ,
	duration         INT  NOT NULL ,
	lang             VARCHAR (50) NOT NULL ,
	subtitles        BOOL  NOT NULL ,
	director         VARCHAR (50) NOT NULL ,
	min_age          INT  NOT NULL ,
	start_date       DATE  NOT NULL ,
	end_date         DATE  NOT NULL ,
	id_movie_owner   INT  NOT NULL  ,
	likes			 INT DEFAULT 0,
	CONSTRAINT movie_PK PRIMARY KEY (id_movie)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: actor
------------------------------------------------------------
CREATE TABLE public.actor(
	id_actor   SERIAL NOT NULL ,
	name       VARCHAR (50) NOT NULL ,
	surname    VARCHAR (50) NOT NULL  ,
	CONSTRAINT actor_PK PRIMARY KEY (id_actor)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: review
------------------------------------------------------------
CREATE TABLE public.review(
	id_review   SERIAL NOT NULL ,
	content     VARCHAR (510) NOT NULL ,
	id_movie	INT NOT NULL ,
	id_user 	INT NOT NULL ,
	reports     INT DEFAULT 0 ,
	CONSTRAINT review_PK PRIMARY KEY (id_review)
)WITHOUT OIDS;


ALTER TABLE public.movie
	ADD CONSTRAINT movie_movie_owner0_FK
	FOREIGN KEY (id_movie_owner)
	REFERENCES public.movie_owner(id_movie_owner)
	ON DELETE CASCADE;

ALTER TABLE public.review
	ADD CONSTRAINT review_movie0_FK
	FOREIGN KEY (id_movie)
	REFERENCES public.movie(id_movie)
	ON DELETE CASCADE;

ALTER TABLE public.review
	ADD CONSTRAINT review_user0_FK
	FOREIGN KEY (id_user)
	REFERENCES public.user(id_user)
	ON DELETE CASCADE;
module.exports = {
  semi: true,
  trailingComma: "none",
  printWidth: 120,
  tabWidth: 2,
  singleQuote: false,
  bracketSpacing: true
};
import MovieOwner from "./dtos/MovieOwner";
import User from "./dtos/User";

/*
  Module augmentation to have user property on JWT decoded payload
*/

declare module "jsonwebtoken" {
  function verify(token: string, secretOrPublicKey: Secret, options?: VerifyOptions): DecodedJwtToken;
  function sign(
    payload: string | object | Buffer,
    secretOrPrivateKey: jwt.Secret,
    option?: jwt.SignOptions | undefined
  ): string;

  export interface DecodedJwtToken {
    user: User | MovieOwner;
  }
}

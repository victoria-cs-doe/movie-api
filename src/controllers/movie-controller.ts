import { findAll, findByName, create, update, findById, remove, like } from "../database/movie-data";
import { BadRequestError, NotFoundError, ResourceConflictError, UnauthorizedError } from "../dtos/Exception";
import { Request, Response, NextFunction } from "express";
import { Movie, verifyMovie } from "../helpers/validate-payload";
import logger from "../middleware/logger";
import { getMovieOwnerId } from "../helpers/identification";
import rollbar from "../middleware/rollbar";

export default class MoviesController {
  findAllMovies = async (req: Request, res: Response, next: NextFunction) => {
    const movies = await findAll();
    if (movies.length === 0) {
      logger.info("No existing movies for this owner");
      res.status(200).json({ message: "No existing movies for this owner" });
      return next();
    }
    const allMovies: Array<Movie> = [];
    movies.forEach((movie) => allMovies.push(movie));
    logger.info("Movies successfully retrieved");
    res.status(200).json(allMovies);
  };

  findMovieByName = async (req: Request, res: Response, next: NextFunction) => {
    const title = req.params.title.toUpperCase();
    const movie = await findByName(title);
    if (!movie) {
      rollbar.error("No existing movie with this title");
      return next(new NotFoundError("No existing movie with this title"));
    }
    logger.info("Movie successfully retrieved");
    res.status(200).json(movie);
  };

  createMovie = async (req: Request, res: Response, next: NextFunction) => {
    const valid = verifyMovie(req.body);
    if (valid.length > 0) {
      rollbar.error(valid.toString());
      return next(new BadRequestError(valid.toString()));
    }
    const newMovie: Movie = req.body as Movie;
    const movie = await findByName(newMovie.title.toUpperCase());
    if (movie) {
      rollbar.error("Movie already exists");
      return next(new ResourceConflictError("Movie already exists"));
    }
    await create(
      newMovie.title.toUpperCase(),
      String(newMovie.duration),
      newMovie.lang,
      String(newMovie.subtitles),
      newMovie.director,
      String(newMovie.min_age),
      newMovie.start_date,
      newMovie.end_date,
      String(getMovieOwnerId(req, res, next))
    );
    logger.info("Movie successfully created !");
    res.status(201).json({ message: "Movie successfully created !" });
  };

  updateMovie = async (req: Request, res: Response, next: NextFunction) => {
    const updatedMovie: Movie = req.body as Movie;
    const id = req.params.id;
    const movie = await findById(id);
    if (!movie) {
      rollbar.error("No existing movie with this id");
      return next(new NotFoundError("No existing movie with this id"));
    }
    if (movie.id_movie_owner !== getMovieOwnerId(req, res, next)) {
      rollbar.error("You can't modify another owner's movies");
      return next(new UnauthorizedError("You can't modify another owner's movies"));
    }
    await update(updatedMovie.title.toUpperCase(), id);
    logger.info("Movie successfully updated !");
    res.status(200).json({ message: "Movie successfully updated !" });
  };

  likeMovie = async (req: Request, res: Response, next: NextFunction) => {
    const movieToLike = req.params.title;
    const movie = await findByName(movieToLike.toUpperCase());
    if (!movie) {
      rollbar.error("No existing movie with this title");
      return next(new NotFoundError("No existing movie with this title"));
    }
    await like(movie.title.toUpperCase());
    logger.info("Movie successfully liked !");
    res.status(200).json({ message: "Movie successfully liked !" });
  };

  deleteMovie = async (req: Request, res: Response, next: NextFunction) => {
    const movieId = req.params.id;
    const movie = await findById(movieId);
    if (!movie) {
      rollbar.error("No existing movie with this id");
      return next(new NotFoundError("No existing movie with this id"));
    }
    if (movie.id_movie_owner !== getMovieOwnerId(req, res, next)) {
      rollbar.error("You can't delete another owner's movies");
      return next(new UnauthorizedError("You can't delete another owner's movies"));
    }
    await remove(movieId);
    logger.info("Movie successfully deleted !");
    res.status(200).json({ message: "Movie successfully deleted !" });
  };
}

import { findAll, findByName, findByNameAndSurname, create } from "../database/actor-data";
import { BadRequestError, NotFoundError, ResourceConflictError } from "../dtos/Exception";
import { Request, Response, NextFunction } from "express";
import { Actor, verifyActor } from "../helpers/validate-payload";
import logger from "../middleware/logger";
import rollbar from "../middleware/rollbar";

export default class ActorsController {
  findAllActors = async (req: Request, res: Response, next: NextFunction) => {
    const actors = await findAll();
    if (actors.length === 0) {
      logger.info("No existing actors in the database");
      res.status(200).json({ message: "No existing actors in the database" });
      return next();
    }
    const allActors: Array<Actor> = [];
    actors.forEach((actor) => allActors.push(actor));
    logger.info("Actors successfully retrieved");
    res.status(200).json(allActors);
  };

  findActorByName = async (req: Request, res: Response, next: NextFunction) => {
    const surname = req.params.surname.toUpperCase();
    const actors = await findByName(surname);
    if (actors.length === 0) {
      rollbar.error("No existing actor with this surname");
      return next(new NotFoundError("No existing actor with this surname"));
    }
    const allActors: Array<Actor> = [];
    actors.forEach((actor) => allActors.push(actor));
    logger.info("Actor(s) successfully retrieved !");
    res.status(200).json(allActors);
  };

  createActor = async (req: Request, res: Response, next: NextFunction) => {
    const valid = verifyActor(req.body);
    if (valid.length > 0) {
      rollbar.error(new BadRequestError(valid.toString()));
      return next(new BadRequestError(valid.toString()));
    }
    const newActor: Actor = req.body as Actor;
    const actor = await findByNameAndSurname(newActor.name, newActor.surname.toUpperCase());
    if (actor) {
      rollbar.error(new ResourceConflictError("Actor already exists !"));
      return next(new ResourceConflictError("Actor already exists !"));
    }
    await create(newActor.name, newActor.surname.toUpperCase());
    logger.info("Actor successfully created !");
    res.status(201).json({ message: "Actor successfully created !" });
  };
}

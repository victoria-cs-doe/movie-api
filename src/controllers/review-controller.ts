import { findAllSingleMovie, findById, report, remove, create } from "../database/review-data";
import { Request, Response, NextFunction } from "express";
import { Review, verifyReview } from "../helpers/validate-payload";
import logger from "../middleware/logger";
import { BadRequestError, NotFoundError, PreconditionRequiredError } from "../dtos/Exception";
import { getRegisteredUserId } from "../helpers/identification";
import rollbar from "../middleware/rollbar";

export default class ReviewsController {
  findAllMovieReviews = async (req: Request, res: Response, next: NextFunction) => {
    const title = req.params.title.toUpperCase();
    const reviews = await findAllSingleMovie(title);
    if (reviews.length === 0) {
      logger.info("No existing reviews for this movie");
      res.status(200).json({ message: "No existing reviews for this movie" });
      return next();
    }
    const allReviews: Array<Review> = [];
    allReviews.forEach((review) => allReviews.push(review));
    logger.info("Reviews successfully retrieved !");
    res.status(200).json(reviews);
  };

  createReview = async (req: Request, res: Response, next: NextFunction) => {
    const valid = verifyReview(req.body);
    if (valid.length > 0) {
      return next(new BadRequestError(valid.toString()));
    }
    const review = req.body as Review;
    await create(review.content, String(review.id_movie), String(getRegisteredUserId(req, res, next)));
    logger.info("Review successfully created !");
    res.status(201).json({ message: "Review successfully created !" });
  };

  reportReview = async (req: Request, res: Response, next: NextFunction) => {
    const reviewId = req.params.id;
    const review = await findById(reviewId);
    if (!review) {
      rollbar.error("No existing review with this id !");
      return next(new NotFoundError("No existing review with this id !"));
    }
    await report(reviewId);
    logger.info("Review successfully reported !");
    res.status(200).json({ message: "Review successfully reported !" });
  };

  deleteReview = async (req: Request, res: Response, next: NextFunction) => {
    const reviewId = req.params.id;
    const review = await findById(reviewId);
    if (!review) {
      rollbar.error("No existing review with this id !");
      return next(new NotFoundError("No existing review with this id !"));
    }
    if (review.reports && review.reports < 3) {
      rollbar.error("Cannot delete a review that hasn't been reported at least three times");
      return next(
        new PreconditionRequiredError("Cannot delete a review that hasn't been reported at least three times")
      );
    }
    await remove(reviewId);
    logger.info("Review successfully deleted !");
    res.status(200).json({ message: "Review successfully deleted !" });
  };
}

import { findOwnerByEmail, findBasicByEmail, createOwner, createRegistered } from "../database/user-data";
import { BadRequestError, NotFoundError, ResourceConflictError, AuthenticationFailureError } from "../dtos/Exception";
import { Request, Response, NextFunction } from "express";
import { MovieOwner, User, verifyMovieOwner, verifyRegisteredUser } from "../helpers/validate-payload";
import { Roles } from "../dtos/Roles";
import { checkRoleValidity, hashPassword, decodeEmailAndPassword, getToken } from "../helpers/validate-privileges";
import logger from "../middleware/logger";
import rollbar from "../middleware/rollbar";

export default class UserController {
  register = async (req: Request, res: Response, next: NextFunction) => {
    const userType = req.params.role;
    if (!checkRoleValidity(userType)) {
      rollbar.error(new BadRequestError("Role should either be owner or registered !"));
      return next(new BadRequestError("Role should either be owner or registered !"));
    }
    if (userType === Roles.registered) {
      const newUser: User = req.body as User;
      const valid = verifyRegisteredUser(req.body);
      if (valid.length > 0) {
        rollbar.error(new BadRequestError(valid.toString()));
        return next(new BadRequestError(valid.toString()));
      }
      const user = await findBasicByEmail(newUser.email);
      if (user) {
        rollbar.error(new ResourceConflictError("User already exists"));
        return next(new ResourceConflictError("User already exists"));
      }
      const hash = await hashPassword(newUser.password);
      await createRegistered(newUser.email, hash);
      logger.info("User successfully created !");
      res.status(201).json({ message: "User successfully created !" });
    } else {
      const newMovieOwner: MovieOwner = req.body as MovieOwner;
      const valid = verifyMovieOwner(req.body);
      if (valid.length > 0) {
        rollbar.error(new BadRequestError(valid.toString()));
        return next(new BadRequestError(valid.toString()));
      }
      const movieOwner = await findOwnerByEmail(newMovieOwner.email);
      if (movieOwner) {
        rollbar.error(new ResourceConflictError("User already exists"));
        return next(new ResourceConflictError("User already exists"));
      }
      const hash = await hashPassword(newMovieOwner.password);
      await createOwner(newMovieOwner.email, hash);
      logger.info("Movie owner successfully created !");
      res.status(201).json({ message: "Movie owner successfully created !" });
    }
  };

  login = async (req: Request, res: Response, next: NextFunction) => {
    const userType = req.params.role;
    if (!checkRoleValidity(userType)) {
      rollbar.error(new BadRequestError("Role should either be owner or registered !"));
      return next(new BadRequestError("Role should either be owner or registered !"));
    }
    const authorization = req.headers.authorization;
    if (!authorization) {
      rollbar.error(new BadRequestError("Please provide Basic auth credentials"));
      return next(new BadRequestError("Please provide Basic auth credentials"));
    }
    const credentials = decodeEmailAndPassword(authorization);
    const email = credentials[0];
    const password = credentials[1];
    if (userType === Roles.owner) {
      const movieOwner = await findOwnerByEmail(email);
      if (!movieOwner) {
        rollbar.error(new NotFoundError("No user found with this email address"));
        return next(new NotFoundError("No user found with this email address"));
      }
      const token = await getToken(movieOwner.id_movie_owner, password, movieOwner.password, userType);
      if (!token) {
        return next(new AuthenticationFailureError("Invalid password"));
      }
      logger.info("Movie owner successfully logged in !");
      res.status(200).json({ owner_token: token });
    } else if (userType === Roles.registered) {
      const user = await findBasicByEmail(email);
      if (!user) {
        return next(new NotFoundError("No user found with this email address"));
      }
      const token = await getToken(user.id_user, password, user.password, userType);
      if (!token) {
        return next(new AuthenticationFailureError("Invalid password"));
      }
      logger.info("User successfully logged in !");
      res.status(200).json({ user_token: token });
    }
  };
}

import { Movie } from "../../helpers/validate-payload";
import { expect, request, movie_owner_id, movie_id } from "../config/helper.spec";
import { owner_token, user_token } from "./user-controller.spec";
import chai from "chai";
import chaiHttp from "chai-http";
import app from "../../app";

chai.use(chaiHttp);

export const getMovie = (id: number | undefined) => {
  const movie: Movie = {
    title: "PULP FICTION",
    duration: 154,
    lang: "eng",
    subtitles: true,
    director: "Quentin Tarantino",
    min_age: 18,
    start_date: "2020-10-10",
    end_date: "2020-10-10",
    id_movie_owner: id
  };
  return movie;
};

describe("Movie controller tests", () => {
  describe("Get all movies", () => {
    it("Should get the list of all movies returns status 200", async () => {
      const response = await request(app).get("/movies");
      expect(response.status).to.equal(200);
    });
  });

  describe("Get movie by title", () => {
    it("Should get the list of movies with provided title returns status 200", async () => {
      const response = await request(app).get("/movies/" + getMovie(movie_owner_id).title);
      expect(response.status).to.equal(200);
    });

    it("Should get the list of movies with provided title returns status 404 when movie doesn't exist", async () => {
      const response = await request(app).get("/movies/DUNKERQUE");
      expect(response.status).to.equal(404);
    });
  });

  describe("Create a movie", () => {
    it("Should create a movie returns status 201", (done) => {
      const movie = {
        title: "TEST",
        duration: 100,
        lang: "eng",
        subtitles: true,
        director: "Test",
        min_age: 12,
        start_date: "2020-10-10",
        end_date: "2020-10-10",
        id_movie_owner: movie_owner_id
      };

      request(app)
        .post("/movies")
        .auth(owner_token, { type: "bearer" })
        .set("Content-Type", "application/json")
        .send(movie)
        .then((response) => {
          expect(response.status).to.equal(201);
        })
        .then(done, done);
      done();
    });

    it("Should create a movie returns status 409 when movie already exists", (done) => {
      request(app)
        .post("/movies")
        .auth(owner_token, { type: "bearer" })
        .set("Content-Type", "application/json")
        .send(getMovie(movie_owner_id))
        .then((response) => {
          expect(response.status).to.equal(409);
        })
        .then(done, done);
      done();
    });

    it("Should create a movie returns status 400 when there are missing fields", (done) => {
      const movie = { title: "TEST" };
      request(app)
        .post("/movies")
        .auth(owner_token, { type: "bearer" })
        .set("Content-Type", "application/json")
        .send(movie)
        .then((response) => {
          expect(response.status).to.equal(400);
        })
        .then(done, done);
      done();
    });
  });

  describe("Update a movie", () => {
    it("Should update a movie returns status 200", (done) => {
      const movie = { title: "TEST" };
      request(app)
        // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
        .patch(`/movies/${movie_id}`)
        .auth(owner_token, { type: "bearer" })
        .set("Content-Type", "application/json")
        .send(movie)
        .then((response) => {
          expect(response.status).to.equal(200);
        })
        .then(done, done);
      done();
    });

    it("Should update a movie returns status 404 when movie does not exist", (done) => {
      const movie = { title: "TEST" };
      request(app)
        .patch("/movies/2000")
        .auth(owner_token, { type: "bearer" })
        .set("Content-Type", "application/json")
        .send(movie)
        .then((response) => {
          expect(response.status).to.equal(200);
        })
        .then(done, done);
      done();
    });
  });

  describe("Like a movie", () => {
    it("Should like a movie returns status 200", (done) => {
      request(app)
        .patch("/movies/likes/" + getMovie(movie_owner_id).title)
        .auth(user_token, { type: "bearer" })
        .set("Content-Type", "application/json")
        .then((response) => {
          expect(response.status).to.equal(200);
        })
        .then(done, done);
      done();
    });

    it("Should like a movie returns status 404 when movie doesn't exist", (done) => {
      request(app)
        .patch("/movies/likes/VICTORIA")
        .auth(user_token, { type: "bearer" })
        .set("Content-Type", "application/json")
        .then((response) => {
          expect(response.status).to.equal(404);
        })
        .then(done, done);
      done();
    });
  });

  describe("Delete a movie", () => {
    it("Should delete a movie returns status 200", (done) => {
      request(app)
        // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
        .delete(`/movies/${movie_id}`)
        .auth(owner_token, { type: "bearer" })
        .set("Content-Type", "application/json")
        .then((response) => {
          expect(response.status).to.equal(200);
        })
        .then(done, done);
      done();
    });

    it("Should delete a movie returns status 404 when movie doesn't exist", (done) => {
      request(app)
        .delete("/movies/2000")
        .auth(owner_token, { type: "bearer" })
        .set("Content-Type", "application/json")
        .then((response) => {
          expect(response.status).to.equal(404);
        })
        .then(done, done);
      done();
    });
  });
});

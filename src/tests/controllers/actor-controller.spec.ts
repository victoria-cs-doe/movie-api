import { Actor } from "../../helpers/validate-payload";
import chai from "chai";
import chaiHttp from "chai-http";
import app from "../../app";
import { owner_token } from "./user-controller.spec";
import { expect, request } from "../config/helper.spec";

chai.use(chaiHttp);

export const getActor = () => {
  const actor: Actor = {
    name: "testname",
    surname: "TESTSURNAME"
  };
  return actor;
};

describe("Actor controller tests", () => {
  describe("Get all actors", () => {
    it("Should get the list of all actors returns status 200", async () => {
      const response = await request(app).get("/actors");
      expect(response.status).to.equal(200);
    });
  });

  describe("Get actor by surname", () => {
    it("Should get the list of actors with provided surname returns status 200", async () => {
      const response = await request(app).get("/actors/" + getActor().surname);
      expect(response.status).to.equal(200);
      expect(response.body).to.have.length(1);
    });

    it("Should get the list of actors with provided surname returns status 404 when actor doesn't exist", async () => {
      const response = await request(app).get("/actors/ROBERTS");
      expect(response.status).to.equal(404);
    });
  });

  describe("Create an actor", () => {
    it("Should create an actor returns status 201", (done) => {
      const actor = { name: "Victoria", surname: "Doe" };
      request(app)
        .post("/actors")
        .set("Authorization", "Bearer " + owner_token)
        .set("Content-Type", "application/json")
        .send(actor)
        .then((response) => {
          expect(response.status).to.equal(201);
        })
        .then(done, done);
      done();
    });

    it("Should create an actor returns status 409 when actor already exists", (done) => {
      request(app)
        .post("/actors")
        .set("Authorization", "Bearer " + owner_token)
        .set("Content-Type", "application/json")
        .send(getActor())
        .then((response) => {
          expect(response.status).to.equal(409);
        })
        .then(done, done);
      done();
    });

    it("Should create an actor returns status 400 when fields are missing", (done) => {
      const actor = { name: "Victoria" };
      request(app)
        .post("/actors")
        .set("Authorization", "Bearer " + owner_token)
        .set("Content-Type", "application/json")
        .send(actor)
        .then((response) => {
          expect(response.status).to.equal(400);
        })
        .then(done, done);
      done();
    });
  });
});

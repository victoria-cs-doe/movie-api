import { Review } from "../../helpers/validate-payload";
import { expect, request, movie_id, movie_owner_id } from "../config/helper.spec";
import { user_token, owner_token } from "./user-controller.spec";
import { getMovie } from "./movie-controller.spec";
import chai from "chai";
import chaiHttp from "chai-http";
import app from "../../app";

chai.use(chaiHttp);

export const getReview = () => {
  const review: Review = {
    content: "Best movie ever !",
    id_movie: movie_id || 0
  };
  return review;
};

describe("Review controller tests", () => {
  describe("Get all reviews", () => {
    it("Should get all the reviews of the requested movie returns status 200", async () => {
      const response = await request(app).get("/reviews/" + getMovie(movie_owner_id).title);
      expect(response.status).to.equal(200);
    });
  });

  describe("Create review", () => {
    it("Should create review returns status 201", (done) => {
      request(app)
        .post("/reviews")
        .set("Authorization", "Bearer " + user_token)
        .set("Content-Type", "application/json")
        .send(getReview())
        .then((response) => {
          expect(response.status).to.equal(201);
        })
        .then(done, done);
      done();
    });

    it("Should create review returns status 400 when there are missing fields", (done) => {
      const review = { content: "Best movie ever !" };
      request(app)
        .post("/reviews")
        .set("Authorization", "Bearer " + user_token)
        .set("Content-Type", "application/json")
        .send(review)
        .then((response) => {
          expect(response.status).to.equal(400);
        })
        .then(done, done);
      done();
    });
  });

  describe("Report review", () => {
    it("Should report review returns status 404 when review does not exist", (done) => {
      request(app)
        .patch("/reviews/report/1000")
        .set("Authorization", "Bearer " + user_token)
        .set("Content-Type", "application/json")
        .then((response) => {
          expect(response.status).to.equal(404);
        })
        .then(done, done);
      done();
    });
  });

  describe("Delete review", () => {
    it("Should report review returns status 404 when review does not exist", (done) => {
      request(app)
        .delete("/reviews/1000")
        .set("Authorization", "Bearer " + owner_token)
        .set("Content-Type", "application/json")
        .then((response) => {
          expect(response.status).to.equal(404);
        })
        .then(done, done);
      done();
    });
  });
});

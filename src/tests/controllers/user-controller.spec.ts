import chai from "chai";
import chaiHttp from "chai-http";
import app from "../../app";
import { Roles } from "../../dtos/Roles";
import { MovieOwner, User } from "../../helpers/validate-payload";
import { expect, request } from "../config/helper.spec";

chai.use(chaiHttp);

export let owner_token = "";
export let user_token = "";

export const createMovieOwner = () => {
  const movieOwner: MovieOwner = {
    email: "example.example@example.com",
    password: "123456",
    role: Roles.owner
  };
  return movieOwner;
};

export const createRegisteredUser = () => {
  const registeredUser: User = {
    email: "example.example@example.com",
    password: "123456",
    role: Roles.registered
  };
  return registeredUser;
};

describe("User controller tests", () => {
  describe("Create a movie owner", () => {
    it("Should create a movie owner returns status 201", async () => {
      const owner = { email: "test.example@example.com", password: "123456" };
      const response = await request(app).post("/users/register/owner").send(owner);
      expect(response.status).to.equal(201);
    });

    it("Should create a movie owner returns status 400 when payload is incomplete", async () => {
      const owner = { email: "email.email@email.com" };
      const response = await request(app).post("/users/register/owner").send(owner);
      expect(response.status).to.equal(400);
    });

    it("Should create a movie owner returns status 409 when user already exists", async () => {
      const response = await request(app).post("/users/register/owner").send(createMovieOwner());
      expect(response.status).to.equal(409);
    });
  });

  describe("Log a movie owner in", () => {
    it("Should log a movie owner in returns status 200", async () => {
      const response = await request(app)
        .post("/users/login/owner")
        .auth(createMovieOwner().email, createMovieOwner().password);
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      owner_token = response.body.owner_token as string;
      expect(response.status).to.equal(200);
    });

    it("Should log a movie owner in returns status 404 when movie owner does not exist", async () => {
      const response = await request(app).post("/users/login/owner").auth("email", createMovieOwner().password);
      expect(response.status).to.equal(404);
    });

    it("Should log a movie owner in returns status 401 when passwords do not match", async () => {
      const response = await request(app).post("/users/login/owner").auth(createMovieOwner().email, "password");
      expect(response.status).to.equal(401);
    });
  });

  describe("Create a registered user", () => {
    it("Should create a registered user returns status 201", async () => {
      const registered = { email: "test.example@example.com", password: "123456" };
      const response = await request(app).post("/users/register/registered").send(registered);
      expect(response.status).to.equal(201);
    });

    it("Should create a registered user returns status 400 when payload is incomplete", async () => {
      const registered = { email: "email.email@email.com" };
      const response = await request(app).post("/users/register/registered").send(registered);
      expect(response.status).to.equal(400);
    });

    it("Should create a registered user returns status 409 when user already exists", async () => {
      const response = await request(app).post("/users/register/registered").send(createRegisteredUser());
      expect(response.status).to.equal(409);
    });
  });

  describe("Log a registered user in", () => {
    it("Should log a registered in returns status 200", async () => {
      const response = await request(app)
        .post("/users/login/registered")
        .auth(createRegisteredUser().email, createRegisteredUser().password);
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      user_token = response.body.user_token as string;
      expect(response.status).to.equal(200);
    });

    it("Should log a registered user in returns status 404 when movie owner does not exist", async () => {
      const response = await request(app)
        .post("/users/login/registered")
        .auth("email", createRegisteredUser().password);
      expect(response.status).to.equal(404);
    });

    it("Should log a registered user in returns status 401 when passwords do not match", async () => {
      const response = await request(app)
        .post("/users/login/registered")
        .auth(createRegisteredUser().email, "password");
      expect(response.status).to.equal(401);
    });
  });
});

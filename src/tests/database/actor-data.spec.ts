import { findAll, findByName, findByNameAndSurname, findById, create } from "../../database/actor-data";
import { getActor } from "../controllers/actor-controller.spec";
import { expect } from "../config/helper.spec";

describe("Actor data tests", () => {
  describe("Find all", () => {
    it("Find all actors should return an array of actors", async () => {
      const response = await findAll();
      expect(response).to.be.an("array");
    });
  });

  describe("Find by name", () => {
    it("Find actor by name should return an array of actors", async () => {
      const response = await findByName(getActor().surname);
      expect(response).to.be.an("array");
    });

    it("Find actor by name and surname should return an actor", async () => {
      const response = await findByNameAndSurname(getActor().name, getActor().surname);
      expect(response).to.be.an("object");
      expect(response.name).to.equal(getActor().name);
      expect(response.surname).to.equal(getActor().surname);
    });

    it("Find by id should return an actor", async () => {
      const actor = await findByNameAndSurname(getActor().name, getActor().surname);
      const response = await findById(String(actor.id_actor));
      expect(response.name).to.equal(getActor().name);
      expect(response.surname).to.equal(getActor().surname);
    });
  });

  describe("Create", () => {
    it("Create actor should return created actor", async () => {
      const actor = { name: "Victoria", surname: "DOE" };
      await create(actor.name, actor.surname);
      const response = await findByNameAndSurname(actor.name, actor.surname);
      expect(response).to.be.an("object");
      expect(response.name).to.equal(actor.name);
      expect(response.surname).to.equal(actor.surname);
    });
  });
});

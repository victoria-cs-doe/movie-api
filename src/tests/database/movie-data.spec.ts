import { findAll, findByName, findById, create, update, like } from "../../database/movie-data";
import { expect } from "../config/helper.spec";
import { getMovie } from "../controllers/movie-controller.spec";
import { movie_owner_id, movie_id } from "../config/helper.spec";

describe("Movie tests", () => {
  describe("Find all", () => {
    it("Find all movies should return array of movies", async () => {
      const response = await findAll();
      expect(response).to.be.an("array");
    });
  });

  describe("Find by name", () => {
    it("Find movie by name should return a movie", async () => {
      const response = await findByName(getMovie(movie_owner_id).title);
      expect(response).to.be.an("object");
      expect(response.min_age).to.equal(getMovie(movie_owner_id).min_age);
      expect(response.director).to.equal(getMovie(movie_owner_id).director);
    });
  });

  describe("Find by id", () => {
    it("Find all movies should return array of movies", async () => {
      const response = await findById(String(movie_id));
      expect(response).to.be.an("object");
      expect(response.title).to.equal(getMovie(movie_owner_id).title);
    });
  });

  describe("Create", () => {
    it("Create movie should return created movie", async () => {
      const movie = {
        title: "DUNKERQUE",
        duration: 107,
        lang: "eng",
        subtitles: true,
        director: "Christopher Nolan",
        min_age: 14,
        start_date: "2020-10-10",
        end_date: "2020-10-10",
        id_movie_owner: movie_owner_id
      };
      await create(
        movie.title,
        String(movie.duration),
        movie.lang,
        String(movie.subtitles),
        movie.director,
        String(movie.min_age),
        movie.start_date,
        movie.end_date,
        String(movie.id_movie_owner)
      );
      const response = await findByName(movie.title);
      expect(response).to.be.an("object");
      expect(response.title).to.equal(movie.title);
    });
  });

  describe("Update", () => {
    it("Update movie should return updated movie", async () => {
      const movie = { title: "POULPE FICTION" };
      await update(movie.title, String(movie_id));
      const response = await findByName(movie.title);
      expect(response).to.be.an("object");
      expect(response.title).to.equal(movie.title);
    });
  });

  describe("Like", () => {
    it("Like movie should return liked movie", async () => {
      const movie = await findByName(getMovie(movie_owner_id).title);
      await like(movie.title);
      const response = await findByName(movie.title);
      expect(response).to.be.an("object");
      expect(response.likes).to.equal(1);
    });
  });
});

import { findAllSingleMovie } from "../../database/review-data";
import { getMovie } from "../controllers/movie-controller.spec";
import { movie_owner_id } from "../config/helper.spec";
import { expect } from "../config/helper.spec";

describe("Review data tests", () => {
  describe("Find all", () => {
    it("Find all movie reviews should return all movie reviews", async () => {
      const response = await findAllSingleMovie(getMovie(movie_owner_id).title);
      expect(response).to.be.an("array");
    });
  });
});

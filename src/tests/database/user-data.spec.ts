import {
  findOwnerByEmail,
  findBasicByEmail,
  findMovieOwnerRole,
  findRegisteredUserRole,
  createOwner,
  createRegistered
} from "../../database/user-data";
import { createMovieOwner, createRegisteredUser } from "../controllers/user-controller.spec";
import { expect, movie_owner_id, registered_user_id } from "../config/helper.spec";
import { Roles } from "../../dtos/Roles";

describe("User tests", () => {
  describe("Find by email", () => {
    it("Find movie owner by email should return movie owner", async () => {
      const response = await findOwnerByEmail(createMovieOwner().email);
      expect(response).to.be.an("object");
      expect(response.email).to.equal(createMovieOwner().email);
    });

    it("Find registered user by email should return registered user", async () => {
      const response = await findBasicByEmail(createRegisteredUser().email);
      expect(response).to.be.an("object");
      expect(response.email).to.equal(createRegisteredUser().email);
    });
  });

  describe("Find role", () => {
    it("Find movie owner role should return movie owner", async () => {
      const response = await findMovieOwnerRole(String(movie_owner_id));
      expect(response).to.be.an("object");
      expect(response.role).to.equal(Roles.owner);
    });

    it("Find registered user role should return registered user", async () => {
      const response = await findRegisteredUserRole(String(registered_user_id));
      expect(response).to.be.an("object");
      expect(response.role).to.equal(Roles.registered);
    });
  });

  describe("Create", () => {
    it("Create movie owner should return movie owner", async () => {
      const owner = { email: "email@email.com", password: "password" };
      await createOwner(owner.email, owner.password);
      const response = await findOwnerByEmail(owner.email);
      expect(response).to.be.an("object");
      expect(response.email).to.equal(owner.email);
    });

    it("Create registered user should return registered user", async () => {
      const registered = { email: "email@email.com", password: "password" };
      await createRegistered(registered.email, registered.password);
      const response = await findBasicByEmail(registered.email);
      expect(response).to.be.an("object");
      expect(response.email).to.equal(registered.email);
    });
  });
});

import { create, deleteAllActors } from "../../database/actor-data";
import { create as createMovie, findByName, deleteAllMovies } from "../../database/movie-data";
import { deleteAllReviews } from "../../database/review-data";
import {
  createRegistered,
  deleteAllMovieOwners,
  deleteAllRegisteredUsers,
  findBasicByEmail,
  findOwnerByEmail
} from "../../database/user-data";
import { createMovieOwner, createRegisteredUser } from "../controllers/user-controller.spec";
import { createOwner } from "../../database/user-data";
import { getMovie } from "../controllers/movie-controller.spec";
import { getActor } from "../controllers/actor-controller.spec";
import { TEST_PASSWORD_HASH } from "../../lib/dotenv";
import chaiHttp from "chai-http";
import chai from "chai";

chai.use(chaiHttp);

export const expect = chai.expect;
export const request = chai.request;
export let movie_owner_id: number | undefined = 0;
export let movie_id: number | undefined = 0;
export let registered_user_id: number | undefined = 0;

beforeEach(async () => {
  //Delete all records
  await deleteAllActors();
  await deleteAllReviews();
  await deleteAllRegisteredUsers();
  await deleteAllMovies();
  await deleteAllMovieOwners();

  //Insert movie owner
  await createOwner(createMovieOwner().email, TEST_PASSWORD_HASH);
  //Insert registered user
  await createRegistered(createRegisteredUser().email, TEST_PASSWORD_HASH);
  //Get registered user id
  await getRegisteredUserId();
  //Insert actor
  await create(getActor().name, getActor().surname);
  //Insert movie
  await createTestMovie();
});

const createTestMovie = async () => {
  const movieOwner = await findOwnerByEmail(createMovieOwner().email);
  const movie = getMovie(movieOwner.id_movie_owner);
  await createMovie(
    movie.title,
    String(movie.duration),
    movie.lang,
    String(movie.subtitles),
    movie.director,
    String(movie.min_age),
    movie.start_date,
    movie.end_date,
    String(movie.id_movie_owner)
  );
  movie_owner_id = movie.id_movie_owner;
  const found = await findByName(movie.title);
  movie_id = found.id_movie;
};

const getRegisteredUserId = async () => {
  const registered = await findBasicByEmail(createRegisteredUser().email);
  registered_user_id = registered.id_user;
};

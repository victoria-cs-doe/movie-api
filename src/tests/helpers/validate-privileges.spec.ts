import chai from "chai";
import chaiHttp from "chai-http";
import { Roles } from "../../dtos/Roles";
import { checkRoleValidity, decodeEmailAndPassword } from "../../helpers/validate-privileges";
import { expect } from "../config/helper.spec";

chai.use(chaiHttp);

describe("Validate privileges tests", () => {
  describe("Role validity tests", () => {
    it("Check role validity should return true", () => {
      const role = Roles.owner;
      const isValid = checkRoleValidity(role);
      expect(isValid).to.be.true;
    });

    it("Check role validity should return false", () => {
      const role = "basic";
      const isValid = checkRoleValidity(role);
      expect(isValid).to.be.false;
    });
  });

  describe("Decode email and password tests", () => {
    it("Should return empty array when credentials are not correctly provided", () => {
      const authorization = "";
      const credentials = decodeEmailAndPassword(authorization);
      expect(credentials).to.have.length(0);
    });
  });
});

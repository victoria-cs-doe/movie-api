import {
  verifyActor,
  verifyMovie,
  verifyReview,
  verifyMovieOwner,
  verifyRegisteredUser
} from "../../helpers/validate-payload";
import { getActor } from "../controllers/actor-controller.spec";
import { getMovie } from "../controllers/movie-controller.spec";
import { createMovieOwner, createRegisteredUser } from "../controllers/user-controller.spec";
import { getReview } from "../controllers/review-controller.spec";
import { expect } from "../config/helper.spec";

describe("Validate payload tests", () => {
  describe("Actor payload tests", () => {
    it("Verify actor payload should return empty array when given right payload", () => {
      const verified = verifyActor(getActor());
      expect(verified).to.have.length(0);
    });

    it("Verify actor payload should return path reporter error when given wrong payload", () => {
      const actorPayload = { name: "Actor name" };
      const verified = verifyActor(actorPayload);
      expect(verified).to.have.length(1);
    });
  });

  describe("Movie payload tests", () => {
    it("Verify movie payload should return empty array when given right payload", () => {
      const verified = verifyMovie(getMovie(1));
      expect(verified).to.have.length(0);
    });

    it("Verify movie payload should path reporter error when given wrong payload", () => {
      const moviePayload = { title: "Movie title" };
      const verified = verifyMovie(moviePayload);
      expect(verified).to.have.length(7); //There are 7 mandatory missing fields
    });
  });

  describe("Movie owner payload tests", () => {
    it("Verify movie owner payload should return empty array when given right payload", () => {
      const verified = verifyMovieOwner(createMovieOwner());
      expect(verified).to.have.length(0);
    });

    it("Verify movie owner payload should return path reporter error when given wrong payload", () => {
      const ownerPayload = { email: "Movie owner email" };
      const verified = verifyMovieOwner(ownerPayload);
      expect(verified).to.have.length(1);
    });

    it("Verify movie owner payload should return error when given payload with wrong email", () => {
      const ownerPayload = { email: "email.email", password: "123456" };
      const verified = verifyMovieOwner(ownerPayload);
      expect(verified).to.have.length(1);
    });
  });

  describe("Registered user payload tests", () => {
    it("Verify registered user payload should return empty array when given right payload", () => {
      const verified = verifyRegisteredUser(createRegisteredUser());
      expect(verified).to.have.length(0);
    });

    it("Verify registered user payload should return path reporter error when given wrong payload", () => {
      const userPayload = { email: "Registered user email" };
      const verified = verifyRegisteredUser(userPayload);
      expect(verified).to.have.length(1);
    });

    it("Verify registered user payload should return error when given payload with wrong email", () => {
      const userPayload = { email: "email.email", password: "123456" };
      const verified = verifyRegisteredUser(userPayload);
      expect(verified).to.have.length(1);
    });
  });

  describe("Review payload tests", () => {
    it("Verify review payload should return empty array when given right payload", () => {
      const verified = verifyReview(getReview());
      expect(verified).to.have.length(0);
    });

    it("Verify review payload should return path reporter error when given wrong payload", () => {
      const reviewPayload = { content: "Review content" };
      const verified = verifyReview(reviewPayload);
      expect(verified).to.have.length(1);
    });
  });
});

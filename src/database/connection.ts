import { QueryResult } from "pg";
import { DatabaseError } from "../dtos/Exception";
import logger from "../middleware/logger";
import rollbar from "../middleware/rollbar";
import client from "../lib/database";

client.connect((err: Error) => {
  if (err) {
    logger.error(err);
    rollbar.error(err);
    throw err;
  }
  logger.info("Connected to postgres database !");
});

export default async function connection<T = string>(request: string, parameters?: Array<string>) {
  return await client
    .query(request, parameters)
    .then((res: QueryResult<T>) => {
      return res.rows;
    })
    .catch((err: Error) => {
      logger.error(err);
      rollbar.error(err);
      throw new DatabaseError();
    });
}

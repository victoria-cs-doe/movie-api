import requests from "./queries";
import connection from "./connection";
import { Review } from "../helpers/validate-payload";

export const findAllSingleMovie = async (title: string): Promise<Array<Review>> => {
  return connection<Review>(requests.FIND_ALL_MOVIE_REVIEWS, [title]);
};

export const findById = async (id: string) => {
  const reviews = await connection<Review>(requests.FIND_REVIEW_BY_ID, [id]);
  return reviews[0];
};

export const create = async (content: string, id_movie: string, id_user: string) => {
  await connection<Review>(requests.ADD_REVIEW, [content, id_movie, id_user]);
};

export const report = async (id: string) => {
  await connection<Review>(requests.REPORT_REVIEW, [id]);
};

export const remove = async (id: string) => {
  await connection<Review>(requests.DELETE_REVIEW, [id]);
};

export const deleteAllReviews = async () => {
  await connection<Review>(requests.DELETE_ALL_REVIEWS);
};

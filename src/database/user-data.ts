import requests from "./queries";
import connection from "./connection";
import { User, MovieOwner } from "../helpers/validate-payload";

export const findOwnerByEmail = async (email: string): Promise<MovieOwner> => {
  const movieOwners = await connection<MovieOwner>(requests.FIND_MOVIE_OWNER_BY_EMAIL, [email]);
  return movieOwners[0];
};

export const findBasicByEmail = async (email: string): Promise<User> => {
  const users = await connection<User>(requests.FIND_REGISTERED_USER_BY_EMAIL, [email]);
  return users[0];
};

export const findMovieOwnerRole = async (id: string): Promise<MovieOwner> => {
  const movieOwners = await connection<MovieOwner>(requests.FIND_MOVIE_OWNER_ROLE, [id]);
  return movieOwners[0];
};

export const findRegisteredUserRole = async (id: string): Promise<MovieOwner> => {
  const movieOwners = await connection<MovieOwner>(requests.FIND_REGISTERED_USER_ROLE, [id]);
  return movieOwners[0];
};

export const createOwner = async (email: string, password: string) => {
  await connection<MovieOwner>(requests.ADD_MOVIE_OWNER, [email, password]);
};

export const createRegistered = async (email: string, password: string) => {
  await connection<User>(requests.ADD_REGISTERED_USER, [email, password]);
};

export const deleteAllRegisteredUsers = async () => {
  await connection<User>(requests.DELETE_ALL_REGISTERED_USERS);
};

export const deleteAllMovieOwners = async () => {
  await connection<MovieOwner>(requests.DELETE_ALL_MOVIE_OWNERS);
};

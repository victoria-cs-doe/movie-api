import requests from "./queries";
import connection from "./connection";
import { Movie } from "../helpers/validate-payload";

export const findAll = async (): Promise<Array<Movie>> => {
  return connection<Movie>(requests.FIND_ALL_MOVIES);
};

export const findByName = async (name: string): Promise<Movie> => {
  const movies = await connection<Movie>(requests.FIND_MOVIE_BY_NAME, [name]);
  return movies[0];
};

export const findById = async (id: string): Promise<Movie> => {
  const movies = await connection<Movie>(requests.FIND_MOVIE_BY_ID, [id]);
  return movies[0];
};

export const create = async (
  title: string,
  duration: string,
  lang: string,
  subtitles: string,
  director: string,
  minAge: string,
  startDate: string,
  endDate: string,
  movieOwnerId: string
) => {
  await connection<Movie>(requests.ADD_MOVIE, [
    title,
    duration,
    lang,
    subtitles,
    director,
    minAge,
    startDate,
    endDate,
    movieOwnerId
  ]);
};

export const update = async (title: string, id_movie: string) => {
  await connection<Movie>(requests.UPDATE_MOVIE, [title, id_movie]);
};

export const like = async (title: string) => {
  await connection<Movie>(requests.LIKE_MOVIE, [title]);
};

export const remove = async (id: string) => {
  await connection<Movie>(requests.DELETE_MOVIE, [id]);
};

export const deleteAllMovies = async () => {
  await connection<Movie>(requests.DELETE_ALL_MOVIES);
};

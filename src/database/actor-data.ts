import requests from "./queries";
import connection from "./connection";
import { Actor } from "../helpers/validate-payload";

export const findAll = async (): Promise<Array<Actor>> => {
  return connection<Actor>(requests.FIND_ALL_ACTORS);
};

export const findByName = async (surname: string): Promise<Array<Actor>> => {
  return connection<Actor>(requests.FIND_ACTOR_BY_SURNAME, [surname]);
};

export const findById = async (id: string): Promise<Actor> => {
  const actors = await connection<Actor>(requests.FIND_ACTOR_BY_ID, [id]);
  return actors[0];
};

export const findByNameAndSurname = async (name: string, surname: string): Promise<Actor> => {
  const actors = await connection<Actor>(requests.FIND_ACTOR_BY_NAME_AND_SURNAME, [name, surname]);
  return actors[0];
};

export const create = async (name: string, surname: string) => {
  await connection<Actor>(requests.ADD_ACTOR, [name, surname]);
};

export const deleteAllActors = async () => {
  await connection<Actor>(requests.DELETE_ALL_ACTORS);
};

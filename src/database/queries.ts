const queries = {
  FIND_ALL_ACTORS: "SELECT * FROM actor",
  FIND_ACTOR_BY_SURNAME: "SELECT * FROM actor WHERE surname = $1",
  FIND_ACTOR_BY_NAME_AND_SURNAME: "SELECT * FROM actor WHERE name = $1 AND surname = $2",
  FIND_ACTOR_BY_ID: "SELECT * FROM actor WHERE id_actor = $1",
  ADD_ACTOR: "INSERT INTO ACTOR (name, surname) VALUES ($1, $2)",

  FIND_ALL_MOVIES: "SELECT * FROM movie",
  FIND_MOVIE_BY_NAME: "SELECT * FROM movie WHERE title = $1",
  ADD_MOVIE:
    "INSERT INTO MOVIE (title, duration, lang, subtitles, director, min_age, start_date, end_date, id_movie_owner)" +
    " VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)",
  UPDATE_MOVIE: "UPDATE movie SET title = $1 where id_movie = $2",
  FIND_MOVIE_BY_ID: "SELECT * from movie where id_movie = $1",
  DELETE_MOVIE: "DELETE FROM MOVIE WHERE id_movie = $1",
  LIKE_MOVIE: "UPDATE movie SET likes = likes + 1 where title = $1",

  FIND_MOVIE_OWNER_BY_EMAIL: "SELECT * FROM movie_owner WHERE email = $1",
  ADD_MOVIE_OWNER: "INSERT INTO movie_owner (email, password) values ($1, $2)",
  FIND_MOVIE_OWNER_ROLE: "SELECT * FROM movie_owner WHERE id_movie_owner = $1",

  FIND_REGISTERED_USER_BY_EMAIL: "SELECT * FROM public.user WHERE email = $1",
  FIND_REGISTERED_USER_ROLE: "SELECT * FROM public.user WHERE id_user = $1",
  ADD_REGISTERED_USER: "INSERT INTO public.user (email, password) values ($1, $2)",

  FIND_ALL_MOVIE_REVIEWS:
    "SELECT r.id_review, r.content, r.id_user FROM review r INNER JOIN movie m ON r.id_movie = m.id_movie WHERE title = $1",
  ADD_REVIEW: "INSERT INTO review (content, id_movie, id_user) values ($1, $2, $3)",
  REPORT_REVIEW: "UPDATE review SET reports = reports + 1 where id_review = $1",
  FIND_REVIEW_BY_ID: "SELECT * FROM review where id_review = $1",
  DELETE_REVIEW: "DELETE FROM review where id_review = $1",

  DELETE_ALL_ACTORS: "DELETE FROM public.actor",
  DELETE_ALL_MOVIES: "DELETE FROM public.movie",
  DELETE_ALL_REVIEWS: "DELETE FROM public.review",
  DELETE_ALL_REGISTERED_USERS: "DELETE FROM public.user",
  DELETE_ALL_MOVIE_OWNERS: "DELETE FROM public.movie_owner"
};

export default queries;

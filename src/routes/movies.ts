import { Router } from "express";
import MoviesController from "../controllers/movie-controller";
import {
  requireAuthentication,
  requireMovieOwnerRole,
  requireRegisteredUserRole
} from "../helpers/validate-privileges";
import { validateRequest } from "../helpers/validate-request";
import { autoCatch } from "../middleware/auto-catch";

const router: Router = Router();
const movieController: MoviesController = new MoviesController();

router.get("/", autoCatch(movieController.findAllMovies));
router.get("/:title", autoCatch(movieController.findMovieByName));
router.post(
  "/",
  [validateRequest, requireAuthentication, requireMovieOwnerRole],
  autoCatch(movieController.createMovie)
);
router.patch("/:id", [requireAuthentication, requireMovieOwnerRole], autoCatch(movieController.updateMovie));
router.patch("/likes/:title", [requireAuthentication, requireRegisteredUserRole], autoCatch(movieController.likeMovie));
router.delete("/:id", [requireAuthentication, requireMovieOwnerRole], autoCatch(movieController.deleteMovie));

export default router;

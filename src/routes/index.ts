import { Router, Request, Response } from "express";
import actorRoutes from "./actors";
import movieRoutes from "./movies";
import userRoutes from "./users";
import reviewRoutes from "./reviews";

const router: Router = Router();
router.use("/actors", actorRoutes);
router.use("/movies", movieRoutes);
router.use("/users", userRoutes);
router.use("/reviews", reviewRoutes);

router.get("/", (req: Request, res: Response) => {
  return res.status(200).json({ message: "Movie Api" });
});

export default router;

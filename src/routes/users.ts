import { Router } from "express";
import UserController from "../controllers/user-controller";
import { autoCatch } from "../middleware/auto-catch";
import { requireCredentials } from "../helpers/validate-privileges";

const router: Router = Router();
const userController: UserController = new UserController();

router.post("/register/:role", autoCatch(userController.register));
router.post("/login/:role", [requireCredentials], autoCatch(userController.login));

export default router;

import { Router } from "express";
import ReviewsController from "../controllers/review-controller";
import { autoCatch } from "../middleware/auto-catch";
import {
  requireAuthentication,
  requireRegisteredUserRole,
  requireMovieOwnerRole
} from "../helpers/validate-privileges";
import { validateRequest } from "../helpers/validate-request";

const router: Router = Router();
const reviewsController: ReviewsController = new ReviewsController();

router.get("/:title", autoCatch(reviewsController.findAllMovieReviews));
router.post(
  "/",
  [validateRequest, requireAuthentication, requireRegisteredUserRole],
  autoCatch(reviewsController.createReview)
);
router.patch(
  "/report/:id",
  [requireAuthentication, requireRegisteredUserRole],
  autoCatch(reviewsController.reportReview)
);
router.delete("/:id", [requireAuthentication, requireMovieOwnerRole], autoCatch(reviewsController.deleteReview));

export default router;

import { Router } from "express";
import { requireAuthentication, requireMovieOwnerRole } from "../helpers/validate-privileges";
import { validateRequest } from "../helpers/validate-request";
import { autoCatch } from "../middleware/auto-catch";
import ActorsController from "../controllers/actor-controller";

const router: Router = Router();
const actorsController: ActorsController = new ActorsController();

router.get("/", autoCatch(actorsController.findAllActors));
router.get("/:surname", autoCatch(actorsController.findActorByName));
router.post(
  "/",
  [validateRequest, requireAuthentication, requireMovieOwnerRole],
  autoCatch(actorsController.createActor)
);

export default router;

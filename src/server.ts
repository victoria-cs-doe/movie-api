import app from "./app";
import { SERVER_PORT } from "./lib/dotenv";
import logger from "./middleware/logger";

//Launch server
const PORT: number = parseInt(SERVER_PORT, 10);

app.listen(PORT, () => {
  logger.info(`Listening on port ${PORT}`);
});

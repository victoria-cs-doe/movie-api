import { Client } from "pg";
import { DATABASE_USER, DATABASE_HOST, DATABASE_NAME, DATABASE_PASSWORD, DATABASE_PORT } from "../lib/dotenv";

const client = new Client({
  user: DATABASE_USER,
  host: DATABASE_HOST,
  database: DATABASE_NAME,
  password: DATABASE_PASSWORD,
  port: parseInt(DATABASE_PORT)
});

export default client;

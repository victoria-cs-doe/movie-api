import * as dotenv from "dotenv";
import path from "path";

export const NODE_ENV = (process.env.NODE_ENV || "development") as "test" | "development" | "production";
if (NODE_ENV === "test") {
  dotenv.config({ path: path.resolve(process.cwd(), `.env.${NODE_ENV}`) });
}
dotenv.config();

const getOrThrow = (name: string) => {
  const val = process.env[name];
  if (typeof val === "undefined") throw new Error(`Missing mandatory environment variable ${name}`);
  return val;
};

export const DATABASE_USER = getOrThrow("DATABASE_USER");
export const DATABASE_PASSWORD = getOrThrow("DATABASE_PASSWORD");
export const DATABASE_NAME = getOrThrow("DATABASE_NAME");
export const DATABASE_HOST = getOrThrow("DATABASE_HOST");
export const DATABASE_PORT = getOrThrow("DATABASE_PORT");
export const SERVER_PORT = getOrThrow("PORT");
export const SECRET = getOrThrow("SECRET");
export const ROLLBAR_ACCESS_TOKEN = getOrThrow("ROLLBAR_ACCESS_TOKEN");
export const TEST_PASSWORD_HASH = getOrThrow("TEST_PASSWORD_HASH");

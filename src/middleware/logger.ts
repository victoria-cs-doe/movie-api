import winston from "winston";
import fs from "fs";
import { NODE_ENV } from "../lib/dotenv";

const levels = {
  error: 0,
  warn: 1,
  info: 2,
  http: 3,
  debug: 4
};

const level = () => {
  const isDevEnv = NODE_ENV === "development";
  return isDevEnv ? "debug" : "warn";
};

const colors = {
  error: "red",
  warn: "yellow",
  info: "green",
  http: "magenta",
  debug: "white"
};

winston.addColors(colors);

const date = new Date();
const year = date.getFullYear();
const month = `0${date.getMonth() + 1}`.slice(-2);
const day = `0${date.getDate()}`.slice(-2);

const directory = __dirname + "/../logs/";
!fs.existsSync(directory) && fs.mkdirSync(directory);

const transports = [
  new winston.transports.File({
    filename: `${directory}${year}-${month}-${day}.error.log`,
    level: "error"
  }),
  new winston.transports.File({
    filename: `${directory}${year}-${month}-${day}.combined.log`
  })
];

const format = winston.format.combine(
  winston.format.colorize({ all: true }),
  winston.format.timestamp({ format: "YYYY-MM-DD HH:mm:ss:ms" }),
  // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
  winston.format.printf((info) => `Timestamp : ${info.timestamp} - Level : ${info.level} - Message : ${info.message}`)
);

const logger = winston.createLogger({
  level: level(),
  levels,
  format: format,
  transports: transports
});

if (NODE_ENV !== "production") {
  logger.add(
    new winston.transports.Console({
      format: winston.format.simple()
    })
  );
}

export default logger;

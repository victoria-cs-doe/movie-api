import rollbar from "rollbar";
import { ROLLBAR_ACCESS_TOKEN } from "../lib/dotenv";

const rollbarLogger = new rollbar({
  accessToken: ROLLBAR_ACCESS_TOKEN,
  captureUncaught: true,
  captureUnhandledRejections: true
});

export default rollbarLogger;

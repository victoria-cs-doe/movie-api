import { Request, Response, NextFunction } from "express";

/*
  Automatically catches errors in the queried route and forwards them to the next middleware or handler
  The program handles them as defined in the code
*/
export const autoCatch = (middleware: (req: Request, res: Response, next: NextFunction) => Promise<void>) => {
  return (req: Request, res: Response, next: NextFunction) => {
    middleware(req, res, next).catch((err) => {
      next(err);
    });
  };
};

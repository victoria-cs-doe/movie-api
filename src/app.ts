import express from "express";
import routes from "./routes/index";
import helmet from "helmet";
import errorHandler from "./middleware/error-handler";
import { jsonHeaders } from "./helpers/validate-request";

const app = express();

//Helpers
app.use(jsonHeaders);
app.use(express.json());
app.use(helmet());

//Routes
app.use("/", routes);

//Middleware
app.use(errorHandler);

export default app;

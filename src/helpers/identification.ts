import { getDecodedPayload } from "./validate-privileges";
import { Request, Response, NextFunction } from "express";
import { User, MovieOwner } from "../helpers/validate-payload";

/*
  Decodes token of current logged in registered user
  This value is automatically injected in the methods so that the user does not need to know their id
  It also prevents them from performing actions on other people's resources
*/
export const getRegisteredUserId = (req: Request, res: Response, next: NextFunction) => {
  const decoded = getDecodedPayload(req, res, next);
  let registered;
  if (decoded) {
    registered = decoded.user as User;
    return registered.id_user;
  }
};

/*
  Decodes token of current logged in movie owner
  This value is automatically injected in the methods so that the user does not need to know their id
  It also prevents them from performing actions on other people's resources
*/
export const getMovieOwnerId = (req: Request, res: Response, next: NextFunction) => {
  const decoded = getDecodedPayload(req, res, next);
  let owner;
  if (decoded) {
    owner = decoded.user as MovieOwner;
    return owner.id_movie_owner;
  }
};

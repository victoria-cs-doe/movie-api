import { BadRequestError } from "../dtos/Exception";
import { Request, Response, NextFunction } from "express";
import logger from "../middleware/logger";
import rollbar from "../middleware/rollbar";

export const jsonHeaders = (req: Request, res: Response, next: NextFunction) => {
  res.set("Content-Type", "application/json");
  res.set("Accept", "application/json");
  next();
};

const requireJSON = (req: Request) => {
  const content = req.headers["content-type"];
  return content === "application/json" || content === "*/*";
};

/*
  Expect all routes to accept and require JSON and only JSON
  The application does not support other types
*/
export const validateRequest = (req: Request, res: Response, next: NextFunction) => {
  if (!requireJSON(req)) {
    rollbar.error("Please make sure the request content type is json");
    logger.error("Please make sure the request content type is json");
    return next(new BadRequestError("Please make sure the request content type is json"));
  }
  return next();
};

import * as t from "io-ts";
import { PathReporter } from "io-ts/PathReporter";
import { isRight } from "fp-ts/lib/Either";
import * as EmailValidator from "email-validator";

const isValidEmail = (email: string) => {
  return EmailValidator.validate(email);
};

const RuntimeActor = t.exact(
  t.intersection([
    t.partial({ id_actor: t.number }),
    t.type({
      name: t.string,
      surname: t.string
    })
  ])
);

const RuntimeMovie = t.exact(
  t.intersection([
    t.partial({ id_movie: t.number, id_movie_owner: t.number, likes: t.number }),
    t.type({
      title: t.string,
      duration: t.number,
      lang: t.string,
      subtitles: t.boolean,
      director: t.string,
      min_age: t.number,
      start_date: t.string,
      end_date: t.string
    })
  ])
);

const RuntimeMovieOwner = t.exact(
  t.intersection([
    t.partial({ id_movie_owner: t.number, role: t.string }),
    t.type({
      email: t.string,
      password: t.string
    })
  ])
);

const RuntimeRegisteredUser = t.exact(
  t.intersection([
    t.partial({ id_user: t.number, role: t.string }),
    t.type({
      email: t.string,
      password: t.string
    })
  ])
);

const RuntimeReview = t.exact(
  t.intersection([
    t.partial({ id_user: t.number, reports: t.number }),
    t.type({
      content: t.string,
      id_movie: t.number
    })
  ])
);

const decodeActor = (body: unknown) => RuntimeActor.decode(body);
const decodeMovie = (body: unknown) => RuntimeMovie.decode(body);
const decodeReview = (body: unknown) => RuntimeReview.decode(body);
const decodeMovieOwner = (body: unknown) => RuntimeMovieOwner.decode(body);
const decodeRegistered = (body: unknown) => RuntimeRegisteredUser.decode(body);

const isValidRegisteredUserBody = (body: unknown): body is User => isRight(decodeRegistered(body));
const isValidMovieBody = (body: unknown): body is Movie => isRight(decodeMovie(body));
const isValidReviewBody = (body: unknown): body is Review => isRight(decodeReview(body));
const isValidActorBody = (body: unknown): body is Actor => isRight(decodeActor(body));
const isValidMovieOwnerBody = (body: unknown): body is MovieOwner => isRight(decodeMovieOwner(body));

/*
  Determines whether the JSON payload is a valid actor or not
*/
export const verifyActor = (body: unknown) => {
  return isValidActorBody(body) ? [] : PathReporter.report(decodeActor(body));
};

/*
  Determines whether the JSON payload is a valid movie or not
*/
export const verifyMovie = (body: unknown) => {
  return isValidMovieBody(body) ? [] : PathReporter.report(decodeMovie(body));
};

/*
  Determines whether the JSON payload is a valid movie owner or not
*/
export const verifyMovieOwner = (body: unknown) => {
  if (isValidMovieOwnerBody(body)) {
    if (isValidEmail(body.email)) {
      return [];
    }
    return ["Please provide a valid email"];
  }
  return PathReporter.report(decodeMovieOwner(body));
};

/*
  Determines whether the JSON payload is a valid registered user or not
*/
export const verifyRegisteredUser = (body: unknown) => {
  if (isValidRegisteredUserBody(body)) {
    if (isValidEmail(body.email)) {
      return [];
    }
    return ["Please provide a valid email"];
  }
  return PathReporter.report(decodeRegistered(body));
};

/*
  Determines whether the JSON payload is a valid review or not
*/
export const verifyReview = (body: unknown) => {
  return isValidReviewBody(body) ? [] : PathReporter.report(decodeReview(body));
};

export type Actor = t.TypeOf<typeof RuntimeActor>;
export type Movie = t.TypeOf<typeof RuntimeMovie>;
export type MovieOwner = t.TypeOf<typeof RuntimeMovieOwner>;
export type Review = t.TypeOf<typeof RuntimeReview>;
export type User = t.TypeOf<typeof RuntimeRegisteredUser>;

import jwt from "jsonwebtoken";
import bcrypt from "bcryptjs";
import { BadRequestError, AuthenticationFailureError, UnauthorizedError } from "../dtos/Exception";
import { Request, Response, NextFunction } from "express";
import { findMovieOwnerRole, findRegisteredUserRole } from "../database/user-data";
import { Roles } from "../dtos/Roles";
import { User, MovieOwner } from "../helpers/validate-payload";
import { SECRET } from "../lib/dotenv";
import logger from "../middleware/logger";
import rollbar from "../middleware/rollbar";

/*
  Encodes user bassword in base64
  Returns encrypted value of base64 password
*/
export const hashPassword = async (password: string) => {
  return await bcrypt.hash(Buffer.from(password, "base64").toString("utf-8"), 10);
};

/*
  Checks if we are trying to log a movie owner or a basic user
*/
export const checkRoleValidity = (role: string) => {
  return Object.keys(Roles).includes(role);
};

/*
  Gets Basic Auth authorization and returns decoded email and password of the user trying to log themselves in
  Basic Auth encodes the credentials in base 64 by default
  Therefore the base64 encoded credentials should be decoded
*/
export const decodeEmailAndPassword = (authorization: string) => {
  if (authorization && authorization.split(" ")[0] === "Basic") {
    const base64Credentials = authorization.split(" ")[1];
    const credentials = Buffer.from(base64Credentials, "base64").toString("utf-8");
    const creds = credentials.split(":");
    return creds.length === 2 ? creds : [];
  }
  return [];
};

/*
  Returns whether hashed password of user input matches the one in the database
*/
const comparePasswords = async (base64password: string, databasePassword: string) => {
  return await bcrypt.compare(base64password, databasePassword);
};

/*
  Encodes a payload according to user role
  Reproduces the same steps as when registering the password in the database
  Compares the hashed password from user input with the one in the database
  If passwords match return the token
*/
export const getToken = async (
  id: number | undefined,
  passwordEntered: string,
  databasePassword: string,
  role: Roles
) => {
  const payload = role === Roles.owner ? { id_movie_owner: id } : { id_user: id };
  const base64password = Buffer.from(passwordEntered, "base64").toString("utf-8");
  if (await comparePasswords(base64password, databasePassword)) {
    return jwt.sign({ user: payload }, SECRET, { expiresIn: "30min" });
  }
};

/*
  Remove the Bearer string from authorization header to only verify the token part
*/
const extractToken = (authorization: string | undefined): string | undefined => {
  return authorization?.split(" ")[1];
};

/*
  Checks whether the user has provided a valid token (the one which was issued)
*/
const verifyToken = (req: Request, res: Response, next: NextFunction) => {
  const isValidToken = getDecodedPayload(req, res, next);
  return isValidToken ? true : false;
};

/*
  Makes sure the users provides a valid token
  @Throws AuthenticationFailureError if the provided token doesn't match the issued one
*/
export const requireAuthentication = (req: Request, res: Response, next: NextFunction) => {
  if (verifyToken(req, res, next)) {
    return next();
  }
};

/*
  Makes sure the user has provided basic auth credentials
  Used for login only
  @Throws BadRequestError if credentials are missing or are not basic auth credentials
*/
export const requireCredentials = (req: Request, res: Response, next: NextFunction) => {
  const authorization = req.headers.authorization;
  if (!authorization || !(authorization.split(" ")[0] === "Basic") || authorization.split(" ")[1].length === 0) {
    logger.error("Please provide basic auth credentials");
    rollbar.error("Please provide basic auth credentials");
    return next(new BadRequestError("Please provide basic auth credentials"));
  }
  return next();
};

/*
  Returns the decoded payload that was encoded in the jwt.sign() method
  @Throws AuthenticationFailureError if token is invalid or expired
*/
export const getDecodedPayload = (req: Request, res: Response, next: NextFunction) => {
  const token = extractToken(req.headers.authorization);
  if (token) {
    try {
      return jwt.verify(token, SECRET);
    } catch (err) {
      logger.error("Invalid or expired token");
      rollbar.error("Invalid or expired token");
      return next(new AuthenticationFailureError("Invalid or expired token"));
    }
  }
};

/*
  Decodes payload from the provided token
  Check if the payload contains a property called id_movie_owner (this means the token was issued to a movie owner)
  Checks in the movie owner table if there is a user with this id that has the movie owner role
  @Throws UnauthorizedError if one of those checks does not pass
*/
export const requireMovieOwnerRole = async (req: Request, res: Response, next: NextFunction) => {
  const decoded = getDecodedPayload(req, res, next);
  if (decoded) {
    const decodedOwner = decoded.user as MovieOwner;
    if (!decodedOwner.id_movie_owner) {
      logger.error("You don't have the sufficient rights to perform this action !");
      rollbar.error("You don't have the sufficient rights to perform this action !");
      return next(new UnauthorizedError("You don't have the sufficient rights to perform this action !"));
    }
    const movieOwner = await findMovieOwnerRole(String(decodedOwner.id_movie_owner));
    if (movieOwner && movieOwner.role === Roles.owner) {
      return next();
    }
  }
};

/*
  Decodes payload from the provided token
  Check if the payload contains a property called id_user (this means the token was issued to a registered user)
  Checks in the user table if there is a user with this id that has the registered user role
  @Throws UnauthorizedError if one of those checks does not pass
*/
export const requireRegisteredUserRole = async (req: Request, res: Response, next: NextFunction) => {
  const decoded = getDecodedPayload(req, res, next);
  if (decoded) {
    const decodedRegistered = decoded.user as User;
    if (!decodedRegistered.id_user) {
      logger.error("You don't have the sufficient rights to perform this action !");
      rollbar.error("You don't have the sufficient rights to perform this action !");
      return next(new UnauthorizedError("You don't have the sufficient rights to perform this action !"));
    }
    const user = await findRegisteredUserRole(String(decodedRegistered.id_user));
    if (user.role === Roles.registered) {
      return next();
    }
  }
};

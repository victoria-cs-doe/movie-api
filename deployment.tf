terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.0"
    }
  }
}
provider "kubernetes" {
  config_path = "~/.kube/config"
}

resource "kubernetes_deployment" "movie-api-deployment" {
  metadata {
    name = "movie-api"
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "movie-api"
      }
    }
    template {
      metadata {
          labels = {
            app = "movie-api"
        }
      }
      spec {
        container {
          image = "2730ef67hbz/movie-api-container:3"
          name  = "movie-api-container"
          image_pull_policy = "Always"
        }
      }
    }
  }
}

resource "kubernetes_service" "movie-api-service" {
  metadata {
    name = "movie-api"
  }
  
  spec {
    selector = {
      app = "movie-api"
    }
    port {
      name = "http"
      port = 4005
      target_port = 4005
    }
    type = "NodePort"
  }
}
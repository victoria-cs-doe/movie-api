FROM node:latest

WORKDIR /home/node/app

COPY package.json .

RUN npm install

ADD . /home/node/app/

EXPOSE 4005

CMD ["npm", "run", "dev"]
